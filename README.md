


````
usage: fbi [ options ] file1 file2 ... fileN

    -h  -help               print this help text 
    -V  -version            print fbi version number 
        -store              write cmd line args to config file 
    -l  -list <arg>         read image filelist from file <arg> 
    -P  -text               switch into text reading mode 
    -a  -autozoom           automagically pick useful zoom factor 
        -(no)autoup           like the above, but upscale only      
        -(no)autodown         like the above, but downscale only    
        -(no)fitwidth         use width only for autoscaling        
    -v  -(no)verbose        show filenames all the time             
    -u  -(no)random         show files in a random order            
    -1  -(no)once           don't loop (for use with -t)            
        -(no)comments       display image comments                  
    -e  -(no)edit           enable editing commands (see man page)  
        -(no)backup           create backup files when editing      
        -(no)preserve         preserve timestamps when editing      
        -(no)readahead      read ahead images into cache            
        -cachemem <arg>     image cache size in megabytes           
        -blend <arg>        image blend time in miliseconds         
    -T  -vt <arg>           start on virtual console <arg>          
    -s  -scroll <arg>       scroll image by <arg> pixels            
    -t  -timeout <arg>      load next image after <arg> sec without user input 
    -r  -resolution <arg>   pick PhotoCD resolution (1..5)          
    -g  -gamma <arg>        set display gamma (doesn't work on all hardware) 
    -f  -font <arg>         use font <arg> (anything fontconfig accepts) 
    -d  -device <arg>       use framebuffer device <arg>            
    -m  -mode <arg>         use video mode <arg> (from /etc/fb.modes) 

Large images can be scrolled using the cursor keys.  Zoom in/out
````


